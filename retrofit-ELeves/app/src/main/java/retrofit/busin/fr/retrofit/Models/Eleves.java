package retrofit.busin.fr.retrofit.Models;

import java.io.Serializable;
import java.util.List;

public class Eleves implements Serializable {

    public List<Eleve> group1 = null;
    public List<Eleve> group2 = null;

    public List<Eleve> getGroup1() {
        return group1;
    }

    public void setGroup1(List<Eleve> group1) {
        this.group1 = group1;
    }

    public List<Eleve> getGroup2() {
        return group2;
    }

    public void setGroup2(List<Eleve> group2) {
        this.group2 = group2;
    }
}