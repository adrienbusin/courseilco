package retrofit.busin.fr.retrofit;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import retrofit.busin.fr.retrofit.Models.Eleves;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Retrofit retrofit1 = new Retrofit.Builder()
                .baseUrl("http://android.busin.fr")
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        EleveService eleveService = retrofit1.create(EleveService.class);

        eleveService.listEleves().enqueue(new Callback<Eleves>() {
            @Override
            public void onResponse(Call<Eleves> call, Response<Eleves> response) {
                Toast.makeText(MainActivity.this, "nombre de repos : " + response.body(), Toast.LENGTH_SHORT).show();

                Eleves eleves = response.body();

            }

            @Override
            public void onFailure(Call<Eleves> call, Throwable t) {
                Toast.makeText(MainActivity.this, "ERROR récupération des eleves", Toast.LENGTH_SHORT).show();
            }
        });

    }
}

