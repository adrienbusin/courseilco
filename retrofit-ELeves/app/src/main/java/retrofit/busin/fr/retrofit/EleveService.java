package retrofit.busin.fr.retrofit;

import java.util.List;

import retrofit.busin.fr.retrofit.Models.Eleves;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by adrien on 16/01/2019.
 */
public interface EleveService {

    @GET("/ing3.json")
    Call<Eleves> listEleves();

}
