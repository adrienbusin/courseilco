package planete.busin.fr.recyclerview;


public class Planete
{
    private String nom;
    private int distance;
    private int idImage;

    Planete(String nom, int distance, int idimage)
    {
        this.nom = nom;
        this.distance = distance;
        this.idImage = idimage;
    }

    public Planete()
    {
        this.nom = "INCONNU";
        this.distance = 0;
        this.idImage = R.drawable.morte;
    }

    public String toString()
    {
        return nom;
    }

    public String getNom()
    {
        return nom;
    }

    public void setNom(String nom)
    {
        this.nom = nom;
    }

    public int getDistance()
    {
        return distance;
    }

    public void setDistance(int distance)
    {
        this.distance = distance;
    }

    public int getIdImage()
    {
        return idImage;
    }

    public void setIdImage(int idImage)
    {
        this.idImage = idImage;
    }
};
