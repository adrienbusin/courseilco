package planete.busin.fr.recyclerview;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by adrien on 16/10/2018.
 */
public class PlaneteAdapter extends RecyclerView.Adapter<PlaneteViewHolder> {

    private List<Planete> mPlanetes;
    private Context mContext;

    PlaneteAdapter(List<Planete> planetes) {
        mPlanetes = planetes;
    }

    @NonNull
    @Override
    public PlaneteViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_planete, viewGroup, false);
        mContext = viewGroup.getContext();
        return new PlaneteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final PlaneteViewHolder holder, int position) {
        holder.bind(position, mContext, mPlanetes);
    }

    @Override
    public int getItemCount() {
        return mPlanetes.size();
    }

}
