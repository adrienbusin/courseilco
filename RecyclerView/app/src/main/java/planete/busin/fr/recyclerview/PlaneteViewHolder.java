package planete.busin.fr.recyclerview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Cache of the children views for a list item.
 */
public class PlaneteViewHolder extends RecyclerView.ViewHolder {

    private ImageView image;
    private TextView distance;
    private TextView nom;

    PlaneteViewHolder(View itemView) {
        super(itemView);
        image = itemView.findViewById(R.id.item_planete_image);
        distance = itemView.findViewById(R.id.item_planete_distance);
        nom = itemView.findViewById(R.id.item_planete_nom);
    }

    public void bind(int position, final Context mContext, List<Planete> mPlanetes) {
        nom.setText(mPlanetes.get(position).getNom());
        distance.setText(String.valueOf(mPlanetes.get(position).getDistance()));
        image.setImageResource(mPlanetes.get(position).getIdImage());

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext, nom.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
