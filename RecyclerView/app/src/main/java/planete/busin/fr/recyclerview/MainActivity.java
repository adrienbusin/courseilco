package planete.busin.fr.recyclerview;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView mRecyclerView = findViewById(R.id.recyclerView);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        List<Planete> mPlanetes = new ArrayList<>();
        mPlanetes.add(new Planete("Jupiter", 741, R.drawable.jupiter));
        mPlanetes.add(new Planete("Mars", 206, R.drawable.mars));
        mPlanetes.add(new Planete("Mercure", 46, R.drawable.mercure));
        mPlanetes.add(new Planete("Neptune", 4445, R.drawable.neptune));
        mPlanetes.add(new Planete("Saturne", 1344, R.drawable.saturn));
        mPlanetes.add(new Planete("Terre", 147, R.drawable.terre));
        mPlanetes.add(new Planete("Uranus", 28000, R.drawable.uranus));
        mPlanetes.add(new Planete("Venus", 107, R.drawable.venus));

        PlaneteAdapter mAdapter = new PlaneteAdapter(mPlanetes);
        mRecyclerView.setAdapter(mAdapter);
    }
}
